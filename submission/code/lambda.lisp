(defn inc [n] (+ n 1))
(defn dec [n] (- n 1))

(defn first [list] (car list))
(defn second [list] (car (cdr list)))
(defn third [list] (car (cdr (cdr list))))

(defn nth-cdr [n list] (if (= n 0) list (nth-cdr (dec n) (cdr list))))

(defn nth [n list] (if (= n 0) (car list) (nth (dec n) (cdr list))))

(defn map [f list]
  (if (atom list) 0
      (cons (f (first list)) (map f (cdr list)))))

(defn append [l1 l2] 
  (if (atom l1) l2
      (cons (first l1)
            (append (cdr l1) l2))))

(defn mapcat [f list]
  (if (atom list) 0
      (append (f (first list)) (mapcat f (cdr list)))))

(defn max [f list]
  (if (atom list) 0
      (let [rmax (max f (cdr list))]
        (if (if (atom rmax) (= rmax 0) 0) (car list) 
            (if (>= (f (car list)) (f rmax)) (car list) rmax)))))

(defn remove [f list]
  (if (atom list) list
      (if (f (car list)) (remove f (cdr list))
          (cons (car list) (remove f (cdr list))))))


(defn bst-node [elt l r] (cons elt (cons l r)))
(defn bst-elt [node] (car node))
(defn bst-l [node] (second node))
(defn bst-r [node] (cdr (cdr node)))

(defn bst-insert [obj node comparer]
  (if (atom node) (bst-node obj 0 0)
      (let [elt (bst-elt node)]
        (if (= (comparer obj elt) 0) node
            (if (< (comparer obj elt) 0)
              (bst-node elt (bst-insert obj (bst-l node) comparer) (bst-r node))
              (bst-node elt (bst-l node) (bst-insert obj (bst-r node) comparer)))))))

(defn bst-insert-all [list node comparer]
  (if (atom list) node
      (bst-insert-all (cdr list) (bst-insert (car list) node comparer) comparer)))

(defn bst-find [obj bst comparer]
  (if (atom bst) 0
      (let [elt (bst-elt bst)]
        (if (= (comparer obj elt) 0) 1
            (if (< (comparer obj elt) 0) (bst-find obj (bst-l bst) comparer)
                (bst-find obj (bst-r bst) comparer))))))

(defn map2d [f area] (map (lambda [row] (map f row)) area))

(defn map-at [map x y] (nth x (nth y map)))

(defn around [x y] (list (list x (inc y) 2)
                         (list (inc x) y 1)
                         (list x (dec y) 0)
                         (list (dec x) y 3)))

(defn find [f list]
  (if (atom list) 0 
      (let [value (f (car list))] (if value value (find f (cdr list))))))

(defn some [f list]
  (if (atom list) 0 
      (let [value (f (car list))] (if value (car list) (some f (cdr list))))))

(defn next-points [amap point end?]
  (if (end? point) (cons 1 point)
      (if (let [value (map-at amap (first point) (second point))]
            (if (= value 1) 1 
                (if (= value 4) 1
                    (if (= value 5) 1 0))))
        (cons 0 (map (lambda [x] (list (first x) (second x) (third point)))
                     (around (first point) (second point))))
        (cons 0 0))))

(defn compare-points [x y] (if (> (car x) (car y)) 1
                               (if (< (car x) (car y)) -1
                                   (if (> (second x) (second y)) 1
                                       (if (< (second x) (second y)) -1
                                           0)))))

(defn search [amap visited queued end? dist]
  (let [queued (remove (lambda [x] (= (bst-find x visited compare-points) 1)) queued)]
    (if (atom queued) 0
        (let [next (map (lambda [point] (next-points amap point end?)) queued)]
          (let [end (some (lambda [x] (= (first x) 1)) next)]
            (if (atom end)
              (search amap (bst-insert-all queued visited compare-points)
                      (mapcat (lambda [x] (cdr x)) next) end?
                      (inc dist))
              (cons dist (nth 3 end))))))))

(defn find-pills2 [wmap points]
  (search wmap 0 points
          (lambda [x] (let [value (map-at wmap (car x) (second x))]
                        (if (= value 2) 1 (if (= value 3) 1 0))))
          0))

(defn find-pills [wmap dir x y]
  (search wmap 0 (cons (list x y dir) 0)
          (lambda [x] (let [value (map-at wmap (car x) (second x))]
                        (if (= value 2) 1 (if (= value 3) 1 0))))
          0))

(defn step [state world]
  (let [wmap (first world)
        status (second world)
        ghosts (third world)
        fruit (nth-cdr 3 world)]
    (let [x (car (second status))
          y (cdr (second status))]
      (let [cells (around x y)]
        (let [pills (find-pills2 wmap (around x y))]
          (cons wmap (if (atom pills) 0 (cdr pills))))))))

(defn setup [world code]
  (cons world step))

(setup world code)
