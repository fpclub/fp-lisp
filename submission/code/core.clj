(ns fp-lisp.core)

(declare compile-atom compile-var compile-sexpr)
(defn compile- [expr]
  (cond (integer? expr) (compile-atom expr)
        (symbol? expr) (compile-var expr)
        :else (compile-sexpr expr)))

(defn compile-atom [expr] [[:LDC expr]])

(defn find-in-env [env var n]
  (if (empty? env) (throw (IllegalArgumentException. (str var)))
      (if-let [id ((first env) var)] [n id]
              (find-in-env (rest env) var (inc n)))))
  
(def ^:dynamic *env* (atom ()))
(defn compile-var [name] 
  (let [[frame id] (find-in-env @*env* name 0)]
    (if (symbol? id)
      [[:LDF id]]
      [[:LD frame id]])))

(defmulti compile-sexpr (fn [[function & args]] (if (sequential? function) :list function)) :default :default)

(defmethod compile-sexpr :default [[fn & args] & rest]
  (if (nil? fn) (throw (IllegalArgumentException.))
      (compile- `(~'funcall ~fn ~@args))))

(defmethod compile-sexpr :list [[fn & args]]
  (compile- `(~'funcall ~fn ~@args)))

(defmacro defmexpr [name [& args] & body]
  `(defmethod compile-sexpr '~name [[_ ~@args]]
     ~@body))

(defmacro defexpr [name [& args] & body]
  `(defmexpr ~name [~@args] 
     (concat ~@(map (fn [arg] `(compile- ~arg)) args)
             [~@body])))

(defmacro defexpr-alias [name [& args] expr]
  `(defmexpr ~name [~@args] (compile-sexpr ~expr)))

(defn- line [v] (str v \newline))

(defexpr + [arg1 arg2] [:ADD])
(defexpr - [arg1 arg2] [:SUB])
(defexpr * [arg1 arg2] [:MUL])
(defexpr / [arg1 arg2] [:DIV])
(defexpr = [arg1 arg2] [:CEQ])
(defexpr > [arg1 arg2] [:CGT])
(defexpr >= [arg1 arg2] [:CGTE])
(defexpr print [value] [:DBUG])
(defexpr-alias < [arg1 arg2] `(~'> ~arg2 ~arg1))
(defexpr-alias <= [arg1 arg2] `(~'>= ~arg2 ~arg1))

(defexpr cons [arg1 arg2] [:CONS])
(defexpr car [arg] [:CAR])
(defexpr cdr [arg] [:CDR])
(defexpr atom [arg] [:ATOM])

(defmexpr list [& args] 
  (if (empty? args) (compile- 0)
      (compile- `(~'cons ~(first args) (~'list ~@(rest args))))))

(def ^:dynamic *labels* nil)

(defmacro label [name & body]
  `(let [name# ~name
         body# (concat ~@body)]
     (swap! *labels* #(conj % [name# body#]))
     name#))

(defmexpr do [& body]
  (doall (mapcat compile- body)))

(defmexpr if [expr then else]
  (when (not then) (throw (IllegalArgumentException. (str expr ":then " then))))
  (when (not else) (throw (IllegalArgumentException. (str expr ":else " else))))
  (let [then-sym (gensym "then")
        else-sym (gensym "else")]
    (concat (compile- expr)
            [[:SEL
              (label then-sym (compile- then) [[:JOIN]])
              (label else-sym (compile- else) [[:JOIN]])]])))

(defmacro with-environment [args expr]
  `(binding [*env* (atom (cons (into {}
                                     (doall (map-indexed #(vector %2 %1) ~args))) @*env*))]
     ~expr))

(defmacro with-named-lambda [[name label] expr]
  `(binding [*env* (atom (cons (conj (first @*env*) [~name ~label])
                               (rest @*env*)))]
     ~expr))

(defmexpr lambda [[& args] expr]
  (let [lambda-sym (label (gensym "lambda")
                          (with-environment args (compile- expr))
                          [[:RTN]])]
    [[:LDF lambda-sym]]))

(defmexpr fn [name [& args] expr]
  (let [lambda-sym (gensym (str "lambda-" name))
        lambda-sym (label lambda-sym
                          (with-environment args
                            (with-named-lambda [name lambda-sym]
                              (compile- expr)))
                          [[:RTN]])]
    [[:LDF lambda-sym]]))

(defmexpr defn [name [& args] expr]
  (let [lambda-sym (gensym (str "lambda-" name))
        lambda-sym (label lambda-sym
                          (with-environment args
                            (with-named-lambda [name lambda-sym]
                              (compile- expr)))
                          [[:RTN]])]
    (swap! *env* (fn [env] (cons (conj (first env) [name lambda-sym])
                                 (rest env))))))

(defmexpr funcall [lambda & args]
  (doall (concat (mapcat compile- args)
                 (compile- lambda)
                 [[:AP (count args)]])))

(defmexpr let [[& bindings] & body]
  (let [names (doall (map first (partition 1 2 bindings)))
        values (doall (map first (partition 1 2 (rest bindings))))]
    (compile- `((~'lambda [~@names] (do ~@body)) ~@values))))

(defn filter-labels [code] 
  (if (empty? code) code
      (let [[op _] (first code)]
        (if (= op :LABEL)
          (let [[label rest-code] (split-with #(and (not= (first %) :JOIN) 
                                                    (not= (first %) :RTN))
                                              code)]
            (cons [(second (first label)) (concat (rest label) [(first rest-code)])]
                  (filter-labels (rest rest-code))))
          (filter-labels (rest code))))))

(defn remove-labels [code got]
  (if (empty? code) got
      (if (= (first (first code)) :LABEL)
        (recur (rest (drop-while #(and (not= (first %) :RTN) (not= (first %) :JOIN)) code)) got)
        (recur (rest code) (conj got (first code))))))

(defn merge-labels [code labels got-labels]
  (if (empty? labels) [code got-labels]
      (merge-labels (concat code (second (first labels))) (rest labels)
                    (conj got-labels [(first (first labels)) (count code)]))))
                    
(defn inject-labels [labels code]
  (map (fn [line] (map #(or (labels %) %) line))
       code))

(defn transform-labels [labels code]
  (let [[code labels] (merge-labels code labels {})]
    (inject-labels labels code)))

(defn compile-all [& exprs]
  (binding [*labels* (atom {})
            *env* (atom (list {'world 0 'code 1}))]
    (doall (map compile- (butlast exprs)))
    (let [lines (concat (compile- (last exprs))
                        [[:RTN]])
          code (transform-labels @*labels* lines)]
      
      (clojure.string/join " " (map (fn [[inst & args]]
                                      (clojure.string/join
                                       " "
                                       (cons (name inst) args))) code)))))

(defn compile-code []
  (let [reader (java.io.PushbackReader. (java.io.FileReader.
                                         "src/fp_lisp/lambda.lisp"))]
    (spit "src/fp_lisp/lambda.gcc"
          (apply compile-all
                 (take-while #(not (nil? %))
                             (repeatedly #(read reader false nil)))))))
          
