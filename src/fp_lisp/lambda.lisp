(defn map2d [f area] (map (lambda [row] (map f row)) area))

(defn spoint [x y dir dist]
  (cons x (cons y (cons dir dist))))

(defn spoint-x [p] (car p))
(defn spoint-y [p] (car (cdr p)))
(defn spoint-dir [p] (car (cdr (cdr p))))
(defn spoint-dist [p] (cdr (cdr (cdr p))))

(defn world-map [world] (first world))

(defn map-at [map point] 
  (nth (spoint-x point) (nth (spoint-y point) map)))

(defn around [point]
  (let [x (spoint-x point)
        y (spoint-y point)]
    (list (spoint x (inc y) 2 0)
          (spoint (inc x) y 1 0)
          (spoint x (dec y) 0 0)
          (spoint (dec x) y 3 0))))

(defn around-dir [point]
  (let [x (spoint-x point)
        y (spoint-y point)
        dir (spoint-dir point)
        dist (spoint-dist point)]
    (list (spoint x (inc y) dir (inc dist))
          (spoint (inc x) y dir (inc dist))
          (spoint x (dec y) dir (inc dist))
          (spoint (dec x) y dir (inc dist)))))

(defn map-at2 [table point]
  (second (lookup (second (lookup table (cdr point)))
                  point)))

(defn ghost-at [world cell]
  (let [ghosts-state (third world)]
    (some (lambda [x] (let [gp (second x)] (if (= (car gp) (spoint-x cell))
                                             (if (= (cdr gp) (spoint-y cell))
                                               (if (= (first x) 2)
                                                 0 1) 0) 0))) ghosts-state)))

(defn cell-info [world cell] 
  (if (atom (ghost-at world cell))
        (let [value (map-at (world-map world) cell)]
          (if (= value 4) (list 0 0 0 cell)
              (if (= value 2) (list cell 0 0 0)
                  (if (= value 3) (list 0 cell 0 0) 0))))
        (list 0 0 cell 0)))

(defn nearest [p1 p2]
  (if (atom p1) p2
      (if (atom p2) p1
          (if (< (spoint-dist p1) (spoint-dist p2)) p1 p2))))

(defn merge-direction [info result]
  (if (atom result) info
      (list (nearest (first info) (first result))
            (nearest (second info) (second result))
            (nearest (third info) (third result))
            (if (atom (nth 3 result)) (nth 3 info) (nth 3 result)))))

(defn merge-cell-info [dir info result n] 
  (if (atom info) result
      (if (= n dir) (cons (merge-direction info (first result))
                          (cdr result))
          (cons (first result) (merge-cell-info dir info (cdr result) (inc n))))))

(defn next-points [world point result]
  (let [value (merge-cell-info (spoint-dir point) (cell-info world point) result 0)]
    (cons value (if (map-at (world-map world) point) (around-dir point) 0))))

(defn compare-points [x y]
  (if (> (spoint-y x) (spoint-y y)) 1
      (if (< (spoint-y x) (spoint-y y)) -1
          (if (> (spoint-x x) (spoint-x y)) 1
              (if (< (spoint-x x) (spoint-x y)) -1 0)))))

(defn search1 [world visited point result]
  (if (atom (lookup visited point))
    (next-points world point result)
    (cons result 0)))

(defn search-generation [world visited queued new-queued result]
  (if (atom queued) (list result new-queued visited)
      (let [visit (car queued) rest (cdr queued)]
        (let [next (search1 world visited visit result)]
          (search-generation world (insert visited visit) rest (append (cdr next) new-queued)
                             (car next))))))

(defn search [world visited queued limit result]
  (if (atom queued) (list result visited queued)
      (if (= limit 0) (list result visited queued)
          (let [ngen (search-generation world visited queued 0 result)]
            (search world (third ngen) (second ngen) (dec limit) (car ngen))))))

(defn find-pills [world points depth]
  (search world (table compare-points) points depth (list 0 0 0 0)))

(defn insert-row [table row begin end]
  (if (>= begin end) table
      (let [middle (/ (+ begin end) 2)]
        (insert-row (insert-row (insert table (list middle (nth middle row)))
                                row begin middle)
                    row (inc middle) end))))

(defn comparer [x y] (if (< (first x) (first y)) -1 (if (> (first x) (first y)) 1 0)))

(defn map-table [world]
  (let [rows (map (lambda [r] (insert-row (table comparer) r 0 (length r))) world)]
    (insert-row (table comparer) rows 0 (length rows))))

(defn best [f list]
  (if (atom list) 0
      (if (atom (cdr list)) (first list)
          (let [rest-best (best f (cdr list))]
            (if (f (first list) rest-best) (first list) rest-best)))))

(defn deadend? [d x y world]
  (let [point (some (lambda [x] (= (spoint-dir x) d)) (around (spoint x y 0 0)))]
    (= (map-at (world-map world) point) 0)))

(defn trace [value] (print value) value)

(defn backward [n] (if (= n 0) 2 (if (= n 1) 3 (if (= n 2) 0 (if (= n 3) 1 -1)))))

(defn score [dir d x y world ghostly?]
  (- (- (if (atom dir) 0
            (let [pill-score (if (atom (first dir)) 0 (- 15 (spoint-dist (first dir))))
                  ghost-score (if (atom (third dir)) 0 (- 45 (* 3 (spoint-dist (third dir)))))
                  power-score (if ghostly? (if (atom (second dir)) 0
                                               (- 30 (* 2 (spoint-dist (second dir))))) 0)
                  cherry-score (if (atom (nth 3 dir)) 0 (/ (* 3 (- 20 (spoint-dist (nth 3 dir)))) 5))]
              (- (+ (if (> (nth-cdr 3 world) 150) cherry-score 0)
                    (+ pill-score power-score))
                 (if (> (first (second world)) 150) (- 0 ghost-score) ghost-score))))
        (if (deadend? d x y world) 100 0))
     (if (= d (backward (third (second world)))) 1 0)))

(defn mod [x n]
  (if (>= x n) (mod (- x (* (/ x n) n)) n) x))

(defn any-random [state] (mod (+ (* 1366 state) 150889) 714024))

(defn all-max [f l]
  (if (atom l) 0
      (if (atom (cdr l)) (list (first l))
          (let [rest (all-max f (cdr l))]
            (if (> (f (first l)) (f (first rest)))
              (list (first l))
              (if (= (f (first l)) (f (first rest)))
                (cons (first l) rest)
                rest))))))

(defn step [state world]
  (let [wmap (first world)
        status (second world)
        ghosts (third world)
        fruit (nth-cdr 3 world)]
    (let [x (car (second status))
          y (cdr (second status))]
      (let [pill-points (car (find-pills world (around (spoint x y 0 0)) 10))]
        (let [ghostly? (find (lambda [x] (if (atom x) 0 (if (atom (third x)) 0 1))) pill-points)]
          (let [scores (map2 (lambda [d dir] (list (score d dir x y world ghostly?) d dir))
                             pill-points (list 0 1 2 3))]
            (let [winners (all-max first scores)
                  state (any-random state)]
              (let [winner (nth (mod (/ state 1024) (length winners)) winners)]
                (cons state (third winner))))))))))

(defn setup [world code]
  (cons 42 step))

(setup world code)
