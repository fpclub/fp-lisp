(defn inc [n] (+ n 1))
(defn dec [n] (- n 1))

(defn first [list] (car list))
(defn second [list] (car (cdr list)))
(defn third [list] (car (cdr (cdr list))))

(defn nth-cdr [n list] (if (= n 0) list (nth-cdr (dec n) (cdr list))))

(defn nth [n list] (if (= n 0) (car list) (nth (dec n) (cdr list))))

(defn length [list] (if (atom list) 0 (inc (length (cdr list)))))

(defn map [f list]
  (if (atom list) 0
      (cons (f (first list)) (map f (cdr list)))))

(defn map2 [f l1 l2]
  (if (atom l1) 0
    (if (atom l2) 0
      (cons (f (car l1) (car l2)) (map2 f (cdr l1) (cdr l2))))))

(defn append [l1 l2] 
  (if (atom l1) l2
      (cons (first l1)
            (append (cdr l1) l2))))

(defn mapcat [f list]
  (if (atom list) 0
      (append (f (first list)) (mapcat f (cdr list)))))

(defn max [f list]
  (if (atom list) 0
      (let [rmax (max f (cdr list))]
        (if (if (atom rmax) (= rmax 0) 0) (car list) 
            (if (>= (f (car list)) (f rmax)) (car list) rmax)))))

(defn remove [f list]
  (if (atom list) list
      (if (f (car list)) (remove f (cdr list))
          (cons (car list) (remove f (cdr list))))))

(defn bst-node [elt l r] (cons elt (cons l r)))
(defn bst-elt [node] (car node))
(defn bst-l [node] (second node))
(defn bst-r [node] (cdr (cdr node)))

(defn bst-insert [obj node comparer]
  (if (atom node) (bst-node obj 0 0)
      (let [elt (bst-elt node)]
        (if (= (comparer obj elt) 0) node
            (if (< (comparer obj elt) 0)
              (bst-node elt (bst-insert obj (bst-l node) comparer) (bst-r node))
              (bst-node elt (bst-l node) (bst-insert obj (bst-r node) comparer)))))))

(defn bst-insert-all [list node comparer]
  (if (atom list) node
      (bst-insert-all (cdr list) (bst-insert (car list) node comparer) comparer)))

(defn bst-find [obj bst comparer]
  (if (atom bst) 0
      (let [elt (bst-elt bst)]
        (if (= (comparer obj elt) 0) elt
            (if (< (comparer obj elt) 0) (bst-find obj (bst-l bst) comparer)
                (bst-find obj (bst-r bst) comparer))))))

(defn table [comparer] (cons 0 comparer))
(defn insert [table value] (cons (bst-insert value (car table) (cdr table)) (cdr table)))
(defn insert-all [table values] (cons (bst-insert-all values (car table) (cdr table)) (cdr table)))
(defn lookup [table value] (bst-find value (car table) (cdr table)))

(defn find [f list]
  (if (atom list) 0 
      (let [value (f (car list))] (if value value (find f (cdr list))))))

(defn some [f list]
  (if (atom list) 0 
      (let [value (f (car list))] (if value (car list) (some f (cdr list))))))


